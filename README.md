# Sample app - Traveller

An example application using Spring Boot, Websocket protocol, Angular and Google Maps in order to display users' visited places.

The project was bootstrapped with https://github.com/oasp/oasp4js-ng-boot-project-seed

| Branch |                                                                                         Pipeline                                                                                         |                                                                                      Code coverage                                                                                       |                                        Test report                                         |
|:------:|:----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------:|:----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------:|:------------------------------------------------------------------------------------------:|
| master | [![pipeline status](https://gitlab.com/ShowMeYourCodeYouTube/traveller-project/badges/master/pipeline.svg)](https://gitlab.com/ShowMeYourCodeYouTube/traveller-project/-/commits/master) | [![coverage report](https://gitlab.com/ShowMeYourCodeYouTube/traveller-project/badges/master/coverage.svg)](https://gitlab.com/ShowMeYourCodeYouTube/traveller-project/-/commits/master) | [link](https://showmeyourcodeyoutube.gitlab.io/traveller-project/test-report) |


---

![Demo](./docs/demo.gif)

## Technology

- JDK 11
- Spring Boot 2
- Data REST
- Websocket
- Jetty
- Maven
- MongoDB (de.flapdoodle.embed)
- Junit
- External services
  - Google Maps
    - https://mapsplatform.google.com/pricing/
    - The API key is available on Gitlab (only for authorized users)
- Angular 6
- yarn
- npm libraries
  - AGM - Angular Google Maps
  - Angular Material
    - https://material.angular.io/
    - https://github.com/angular/flex-layout/wiki
- IntelliJ plugins (optional)
  - Mongo Explorer

## Angular structure

The project is based on: https://medium.com/@motcowley/angular-folder-structure-d1809be95542

## Websocket protocol

References: 
- https://docs.spring.io/spring-framework/docs/4.3.x/spring-framework-reference/html/websocket.html
- https://github.com/sockjs/sockjs-client

![websocket](./docs/message-flow-broker-relay.png)

Traveller setup:
- `/chat-messaging`
  - is the HTTP URL for the endpoint to which a WebSocket (or SockJS) client will need to connect to for the WebSocket handshake.
- `/app` (currently not in use)
  - STOMP messages whose destination header begins with "/app" are routed to @MessageMapping methods in @Controller classes.
- `/chat/{userId}`
  - Use the built-in, message broker for subscriptions and broadcasting; Route messages whose destination header begins with "/chat" to the broker.



## Getting Started

1. Start the backend.
2. Start the frontend (`start-proxy`)
3. Enter: `http://localhost:4200/map/5ae47b387f71d71e00e8db5a`

## Google Java Format

https://github.com/google/styleguide/blob/gh-pages/intellij-java-google-style.xml

Recommended plugin for IntelliJ to automatically format changed files - `Save Actions`.

## Combining frontend & backend - details

### Create history API fallback

Such handling is necessary because the HTML5 history API is used in the client.

``` java
@Controller
public class HistoryApiFallbackController {

  @RequestMapping(value = "app/**", method = RequestMethod.GET)
  public String historyApiFallback() {
    return "forward:/";
  }
}
```

### Display build metadata

``` html
<head>
    ...
    <meta name="version" content="${project.version}">
    <meta name="timestamp" content="${timestamp}">
    ...
</head>
```


```xml
<profile>
    <id>jsclient</id>
    <activation>
        <activeByDefault>true</activeByDefault>
    </activation>
    <build>
        <plugins>
            ...
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-resources-plugin</artifactId>
                <executions>
                    <execution>
                        <id>filter-index.html</id>
                        <phase>generate-resources</phase>
                        <goals>
                            <goal>copy-resources</goal>
                        </goals>
                        <configuration>
                            <useDefaultDelimiters>true</useDefaultDelimiters>
                            <outputDirectory>${project.build.directory}/client</outputDirectory>
                            <resources>
                                <resource>
                                    <directory>${js.client.dir}/dist</directory>
                                    <filtering>true</filtering>
                                    <includes>
                                        <include>index.html</include>
                                    </includes>
                                </resource>
                            </resources>
                        </configuration>
                    </execution>
                    <execution>
                        <id>copy-index.html</id>
                        <phase>generate-resources</phase>
                        <goals>
                            <goal>copy-resources</goal>
                        </goals>
                        <configuration>
                            <outputDirectory>${js.client.dir}/dist</outputDirectory>
                            <overwrite>true</overwrite>
                            <resources>
                                <resource>
                                    <directory>${project.build.directory}/client</directory>
                                    <filtering>false</filtering>
                                    <includes>
                                        <include>index.html</include>
                                    </includes>
                                </resource>
                            </resources>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
        </plugins>
    </build>
</profile>
```
Technically, a copy of `index.html` is created: first the file is filtered and copied to `target/client` and then copied to `src/main/client/dist` overwriting the previous version.
