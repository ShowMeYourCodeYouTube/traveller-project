export const environment = {
  production: true,
  urls: {
    sockjsEndpoint: 'xxx',
    websocketChatTopic: '/chat'
  },
  apiKeyMaps: 'xxx'
};
