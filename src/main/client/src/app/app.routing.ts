import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {DefaultComponent} from './core/components/default/default.component';

export const APP_ROUTES: Routes = [
  {
    path: 'map',
    loadChildren: './modules/map/map.module#MapModule'
  },
  {
    path: '**',
    component: DefaultComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(APP_ROUTES)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
