import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {AppRoutingModule} from './app.routing';
import {HttpClientModule} from '@angular/common/http';
import {NavbarComponent} from './core/components/navbar/navbar.component';
import {EntryService} from './core/services/entry.service';
import {WebsocketService} from './core/services/websocket.service';
import {DatePipe} from '@angular/common';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {DefaultComponent} from './core/components/default/default.component';
import {AppAngularMaterialModule} from './shared/app-angular-material.module';

@NgModule({
  declarations: [
    AppComponent,
    DefaultComponent,
    NavbarComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    AppRoutingModule,
    AppAngularMaterialModule
  ],
  providers: [
    WebsocketService,
    EntryService,
    DatePipe
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
