import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {AppAngularMaterialModule} from './app-angular-material.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    AppAngularMaterialModule
  ],
  exports: [
    CommonModule,
    ReactiveFormsModule,
    AppAngularMaterialModule
  ],
  declarations: []
})
export class SharedModule { }
