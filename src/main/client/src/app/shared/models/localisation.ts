export class Localisation {
  place: string;
  latitude: number;
  longitude: number;

  constructor(place: string, latitude: number, longitude: number) {
    this.place = place;
    this.latitude = latitude;
    this.longitude = longitude;
  }
}
