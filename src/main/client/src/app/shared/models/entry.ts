import {Localisation} from './localisation';

export interface Entry {
  description: string;
  userId: string;
  date: string;
  localisation: Localisation;
  name: string;
  isHidden: boolean;
}
