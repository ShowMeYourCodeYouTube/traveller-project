import {Entry} from './entry';

export interface User {
  userId: string;
  firstName: string;
  lastName: string;
  historyEntries: Entry[];
}
