import {Component, ViewChild} from '@angular/core';
import {Router} from '@angular/router';
import {MatMenuTrigger} from '@angular/material/menu';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent {

  @ViewChild(MatMenuTrigger) trigger: MatMenuTrigger;

  constructor(private router: Router) {
  }

  handleUserChange($event: any, userId: string) {
    $event.stopPropagation();
    this.router.navigate(['map', userId])
      .then(() => {
        this.trigger.closeMenu();
      });
  }
}
