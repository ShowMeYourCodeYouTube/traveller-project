import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Entry} from '../../shared/models/entry';
import {Observable} from 'rxjs';
import {DatePipe} from '@angular/common';
import {map} from 'rxjs/operators';

@Injectable()
export class EntryService {

  constructor(private httpClient: HttpClient, private datePipe: DatePipe) {
  }

  getUserEntries(userId: string): Observable<Entry[]> {
    return this.httpClient.get<UserEntriesResponse>('/entries/search/findByUserId?id=' + userId)
      .pipe(
        map(r => r._embedded.entries)
      );
  }

  addNewEntry(entry: Entry, userId: string): Observable<Entry> {
    entry.userId = userId;
    entry.date = this.datePipe.transform(entry.date, 'yyyy-MM-dd');
    return this.httpClient.post<Entry>('/entries', entry);
  }
}

interface UserEntriesResponse {
  _embedded: {
    entries: Entry[];
    _links: { self: { href: string } };
  };
}
