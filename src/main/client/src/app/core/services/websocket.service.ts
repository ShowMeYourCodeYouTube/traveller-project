import {Injectable} from '@angular/core';
import * as Stomp from 'stompjs';
import {Frame} from 'stompjs';
import * as SockJS from 'sockjs-client';
import {BehaviorSubject, Observable} from 'rxjs';
import {environment} from '../../../environments/environment';

@Injectable()
export class WebsocketService {

  private connectionSource: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  private connecting = false;
  public client;
  private socket;

  initializeWebSocketConnection(): Observable<boolean> {
    return this.connect();
  }

  private connect(): Observable<boolean> {
    if (!this.connecting && !this.isConnected()) {
      this.initConnection();
      this.connecting = true;
    }

    return this.connectionSource.asObservable();
  }

  createSubscription(topic: string, p): Observable<boolean> {
    return this.client.subscribe(topic, p);
  }

  disconnect() {
    if (this.isConnected()) {
      this.client.disconnect();
      this.socket.close();
      this.connectionSource.next(false);
    }
  }

  private isConnected(): boolean {
    return this.socket && this.client && this.client.connected;
  }

  private initConnection() {
    this.socket = new SockJS(environment.urls.sockjsEndpoint);
    this.client = Stomp.over(this.socket);

    this.client.connect({},
      (frame: Frame) => {
        console.log('Frame: ', frame);
        this.connectionSource.next(true);
        this.connecting = false;
      },
      (error: string) => {
        console.log('Error: ', error);
        this.disconnect();
        this.connectionSource.next(false);
        this.connecting = false;
      });
  }
}
