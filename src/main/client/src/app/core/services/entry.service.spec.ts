import {inject, TestBed} from '@angular/core/testing';

import {EntryService} from './entry.service';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {Entry} from '../../shared/models/entry';
import {Localisation} from '../../shared/models/localisation';
import {DatePipe} from '@angular/common';

describe('EntryService', () => {

  const USER_ID = 'U2sCSsd8S&D';
  const USER_ENTRIES: Array<Entry> = [
    {
      description: 'example description1',
      date: '2018-02-06',
      localisation: {
        place: 'Wroclaw',
        latitude: 12.232323,
        longitude: 222.321313
      },
      name: 'name1',
      isHidden: false
    },
    {
      description: 'example description2',
      date: '2018-02-16',
      localisation: {
        place: 'Szczecin',
        latitude: 12.232323,
        longitude: 222.321313
      },
      name: 'name2',
      isHidden: false
    }
  ];
  const newEntry: Entry = {
    description: 'example description3',
      date: '2018-02-16',
    localisation: {
    place: 'Szczecin',
      latitude: 12.232323,
      longitude: 222.321313
  },
    name: 'name3',
      isHidden: false
  };

  let httpMock: HttpTestingController;
  let service: EntryService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [EntryService, DatePipe]
    });
    httpMock = TestBed.get(HttpTestingController);
    service = TestBed.get(EntryService);
  });

  it('should be created', function () {
    expect(service).toBeTruthy();
  });

  it('should get user entries', function () {
    service.getUserEntries(USER_ID).subscribe((data: Array<Entry>) => {
      expect(data.length).toBe(2);
    });

    const req = httpMock.expectOne('api/entry/' + USER_ID);
    expect(req.request.method).toBe('GET');
    req.flush(USER_ENTRIES);
    httpMock.verify();
  });

  it('should add new entry', function () {
    service.addNewEntry(newEntry, USER_ID).subscribe((entry: Entry) => {
      expect(entry).toBe(newEntry);
    });

    const req = httpMock.expectOne('api/entry/' + USER_ID);
    expect(req.request.method).toBe('POST');
    req.flush(newEntry);
    httpMock.verify();
  });

});
