import {Component, EventEmitter, OnDestroy, OnInit, Output} from '@angular/core';
import {Localisation} from '../../../../shared/models/localisation';
import {FormGroup} from '@angular/forms';
import {EntryService} from '../../../../core/services/entry.service';
import {ActivatedRoute} from '@angular/router';
import {Subscription} from 'rxjs';
import {MatDialog} from '@angular/material/dialog';
import {MapEntryFormComponent} from '../map-entry-form/map-entry-form.component';


@Component({
  selector: 'app-map-entry-form-wrapper',
  templateUrl: './map-entry-form-wrapper.component.html',
  styleUrls: ['./map-entry-form-wrapper.component.scss']
})
export class MapEntryFormWrapperComponent implements OnInit, OnDestroy {

  @Output() changePositionOnMap: EventEmitter<Localisation> = new EventEmitter<Localisation>();
  @Output() showAllOnMap: EventEmitter<any> = new EventEmitter<any>();

  userId: string;
  newEntryForm: FormGroup;
  private subscription: Subscription = new Subscription();

  constructor(private entryService: EntryService,
              private route: ActivatedRoute,
              public dialog: MatDialog) {
  }

  ngOnInit() {
    this.setUserId();
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(MapEntryFormComponent, {
    });

    dialogRef.afterClosed().subscribe(formValues => {
      console.log('The dialog was closed');
      if (formValues) {
        this.addEntry(formValues);
      }
    });
  }

  private addEntry(formValues: any) {
    this.entryService.addNewEntry(formValues, this.userId).subscribe();
    this.showAllOnMap.emit(true);
  }

  private setUserId() {
    this.subscription.add(
      this.route.params.subscribe(params => this.userId = params['userId'])
    );
  }
}
