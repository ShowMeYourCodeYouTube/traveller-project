// import {async, ComponentFixture, TestBed} from '@angular/core/testing';
//
// import {AddEntryFormComponent} from './add-entry-form.component';
// import {EntryService} from '../../../../core/services/entry.service';
// import {MockEntryService} from '../../mocks/MockEntryService';
// import {ReactiveFormsModule} from '@angular/forms';
// import {AppAngularMaterialModule} from '../../../../shared/app-angular-material.module';
// import {ActivatedRoute, Router} from '@angular/router';
// import {ActivatedRouteMock, RouterMock} from '../../mocks/Routes';
// import {MapsAPILoader} from '@agm/core';
// import {MockMapsAPILoader} from '../../mocks/MockMapsApiLodaer';
//
// describe('AddEntryFormComponent', () => {
//   let component: AddEntryFormComponent;
//   let fixture: ComponentFixture<AddEntryFormComponent>;
//
//   const exampleNewEntry = {
//     description: 'example description3',
//     date: '2018-02-16',
//     localisation: {
//       place: 'Szczecin',
//       latitude: 12.232323,
//       longitude: 222.321313
//     },
//     name: 'name3',
//     isHidden: false
//   };
//
//   beforeEach(async(() => {
//     TestBed.configureTestingModule({
//       declarations: [AddEntryFormComponent],
//       imports: [
//         ReactiveFormsModule,
//         AppAngularMaterialModule
//       ],
//       providers: [
//         {provide: MapsAPILoader, useClass: MockMapsAPILoader},
//         {provide: EntryService, useClass: MockEntryService},
//         {provide: ActivatedRoute, useClass: ActivatedRouteMock},
//         {provide: Router, useClass: RouterMock}
//       ]
//     })
//       .compileComponents();
//   }));
//
//   beforeEach(() => {
//     fixture = TestBed.createComponent(AddEntryFormComponent);
//     component = fixture.componentInstance;
//     fixture.detectChanges();
//   });
//
//   it('should create component', () => {
//     expect(component).toBeTruthy();
//   });
//
//   it('should add new entry', () => {
//     const inputName = fixture.debugElement.nativeElement.querySelectorAll('#nameInput');
//     const dateInput = fixture.debugElement.nativeElement.querySelectorAll('#dateInput');
//     const locationInput = fixture.debugElement.nativeElement.querySelectorAll('#locationInput');
//     const descriptionInput = fixture.debugElement.nativeElement.querySelectorAll('#descriptionInput');
//     const submitBtn = fixture.debugElement.nativeElement.querySelector('#addEntryBtn');
//     const entryService = fixture.debugElement.injector.get(EntryService);
//     spyOn(entryService, 'addNewEntry');
//
//     inputName.text = exampleNewEntry.name;
//     dateInput.text = exampleNewEntry.date;
//     locationInput.text = exampleNewEntry.localisation.place;
//     descriptionInput.text = exampleNewEntry.description;
//     fixture.detectChanges();
//     submitBtn.click();
//
//     expect(entryService.addNewEntry).toHaveBeenCalled();
//   })
//
// });
