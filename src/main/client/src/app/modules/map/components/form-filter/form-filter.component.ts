import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';

@Component({
  selector: 'app-form-filer',
  templateUrl: './form-filter.component.html',
  styleUrls: ['./form-filter.component.scss']
})
export class FormFilterComponent implements OnInit {

  @Output() dateChanges: EventEmitter<[any, any]> = new EventEmitter<[any, any]>();
  private filterForm: FormGroup;
  private dateFrom: FormControl;
  private dateTo: FormControl;

  constructor() {
  }

  ngOnInit() {
    this.initializeForm();
  }

  initializeForm() {
    this.dateFrom = new FormControl('', null, null);
    this.dateTo = new FormControl('', null, null);
    this.filterForm = new FormGroup({
      dateFrom: this.dateFrom,
      dateTo: this.dateTo
    });
  }

  get form() {
    return this.filterForm
  }

  datesChangeHandler() {
    this.dateChanges.emit([this.dateFrom.value, this.dateTo.value])
  }

  clearFiltering() {
    this.clearDatePickers();
    this.dateChanges.emit(['', ''])
  }

  clearDatePickers() {
    this.dateFrom.setValue('');
    this.dateTo.setValue('');
  }
}
