import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {FormFilterComponent} from './form-filter.component';
import {AppAngularMaterialModule} from '../../../../shared/app-angular-material.module';
import {FormsModule} from '@angular/forms';

describe('FormFilterComponent', () => {
  let component: FormFilterComponent;
  let fixture: ComponentFixture<FormFilterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormFilterComponent ],
      imports: [
        AppAngularMaterialModule,
        FormsModule
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should choose date and clear inputs', () => {
    const button = fixture.debugElement.nativeElement.querySelector('#clearFiltersBtn');
    const dateFromInput = fixture.debugElement.nativeElement.querySelector('#dateFromInput');
    const dateToInput = fixture.debugElement.nativeElement.querySelector('#dateToInput');

    dateFromInput.value = '5/16/2018';
    dateToInput.value = '5/16/2018';

    button.click();

    expect(dateFromInput.value).toBe('');
    expect(dateToInput.value).toBe('');
  });
});
