import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {MapComponent} from './map.component';
import {FormFilterComponent} from '../form-filter/form-filter.component';
import {EntryService} from '../../../../core/services/entry.service';
import {RouterTestingModule} from '@angular/router/testing';
import {ActivatedRoute, Router} from '@angular/router';
import {AgmCoreModule} from '@agm/core';
import {AppAngularMaterialModule} from '../../../../shared/app-angular-material.module';
import {ReactiveFormsModule} from '@angular/forms';
import {MockEntryService} from '../../mocks/MockEntryService';
import {ActivatedRouteMock, RouterMock} from '../../mocks/Routes';

describe('MapComponent', () => {
  let component: MapComponent;
  let fixture: ComponentFixture<MapComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        MapComponent,
        FormFilterComponent
      ],
      imports: [
        AgmCoreModule.forRoot({
          apiKey: 'exampleKey',
          libraries: ['places']
        }),
        RouterTestingModule,
        ReactiveFormsModule,
        AppAngularMaterialModule
      ],
      providers: [
        {provide: EntryService, useClass: MockEntryService},
        {provide: ActivatedRoute, useClass: ActivatedRouteMock},
        {provide: Router, useClass: RouterMock},
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create component and render map', () => {
    expect(component).toBeTruthy();
    const map = fixture.debugElement.nativeElement.querySelector('agm-map');
    expect(map).toBeTruthy();
  });

  it('should show entries on map', () => {
    const markers = fixture.debugElement.nativeElement.querySelectorAll('agm-marker');
    expect(markers.length).toBe(2);
  });
});
