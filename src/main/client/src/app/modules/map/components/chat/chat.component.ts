import {Component, OnInit} from '@angular/core';
import {Entry} from '../../../../shared/models/entry';
import {animate, style, transition, trigger} from '@angular/animations';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.scss'],
  animations: [
    trigger('fadeInOut', [
      transition(':enter', [
        style({opacity: 0,
          transform: 'scale(0.4,0.45)'}),
        animate(500, style({
          opacity: 1,
          transform: 'scale(1,1)'})
        )
      ])
    ])
  ]
})
export class ChatComponent implements OnInit {

  private readonly soundPath = '../../assets/chat-notification.wav';
  soundAfterNewMessage= new Audio();
  newEntries: Array<Entry> = [];

  constructor() {
  }

  ngOnInit() {
    this.soundAfterNewMessage.src = this.soundPath;
  }

  addEntry(entry: Entry) {
    this.newEntries.push(entry);
    this.soundAfterNewMessage.load();
    this.soundAfterNewMessage.play();
  }
}
