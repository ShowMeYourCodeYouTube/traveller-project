import {Component, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {EntryService} from '../../../../core/services/entry.service';
import {Entry} from '../../../../shared/models/entry';
import {FormFilterComponent} from '../form-filter/form-filter.component';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss']
})
export class MapComponent implements OnInit {

  defaultMapLatitude = 52.237049;
  defaultMapLongitude = 21.017532;
  @ViewChild(FormFilterComponent) formFilerView: FormFilterComponent;
  private dateFrom;
  private dateTo;
  private entries: Array<Entry> = [];
  private subscription: Subscription = new Subscription();

  constructor(private route: ActivatedRoute, private entryService: EntryService) {
  }

  ngOnInit() {
    this.subscription.add(
      this.route.params.subscribe(params => this.fetchUserEntries(params['userId']))
    );
  }

  get mapEntries() {
    return this.entries;
  }

  dateChangesHandler([from, to]) {
    this.dateFrom = from;
    this.dateTo = to;
    this.entries.forEach(entry => {
      this.checkIsHiddenEntry(entry);
    });
  }

  addEntry(entry: Entry) {
    this.checkIsHiddenEntry(entry);
    this.entries.push(entry);
  }


  private fetchUserEntries(userId: string) {
    this.entryService.getUserEntries(userId)
      .subscribe(result => this.entries = result);
  }

  private checkIsHiddenEntry(entry) {
    entry.isHidden = (this.dateFrom !== '' && Date.parse(entry.date) < this.dateFrom) ||
      (this.dateTo !== '' && Date.parse(entry.date) > this.dateTo);
  }

  showMarkerDetails(infoWindow, googleMap) {
    if (googleMap.lastOpen != null) {
      googleMap.lastOpen.close();
    }
    googleMap.lastOpen = infoWindow;
    infoWindow.open();
  }

  showAllEntries() {
    this.dateChangesHandler(['', '']);
    this.formFilerView.clearDatePickers();
  }

  trimTooLongDescription(description: string): string {
    let result = description.substring(0, 20);
    if (description.length > 20) {
      result = result + ' ...';
    }
    return result;
  }

  trimTooLongName(title: string): string {
    let result = title.substring(0, 15);
    if (title.length > 15) {
      result = result + ' ...';
    }
    return result;
  }

  setDefaultMapSettings(latitude, longitude) {
    this.defaultMapLatitude = latitude;
    this.defaultMapLongitude = longitude;
  }
}
