import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ChatComponent} from './chat.component';
import {AppAngularMaterialModule} from 'app/shared/app-angular-material.module';
import {ActivatedRoute, Router} from '@angular/router';
import {ActivatedRouteMock, RouterMock} from '../../mocks/Routes';

describe('ChatComponent', () => {
  let component: ChatComponent;
  let fixture: ComponentFixture<ChatComponent>;

  const exampleNewEntry = {
    description: 'example description3',
    date: '2018-02-16',
    localisation: {
      place: 'Szczecin',
      latitude: 12.232323,
      longitude: 222.321313
    },
    name: 'name3',
    isHidden: false
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ChatComponent],
      imports: [
        AppAngularMaterialModule
      ],
      providers: [
        {provide: ActivatedRoute, useClass: ActivatedRouteMock},
        {provide: Router, useClass: RouterMock}
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    if (!(<any>window).Audio) {
      (<any>window).Audio = function() {
        return {
          play: function() {},
          load: function() {},
        };
      };
    }
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ChatComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should add entry', () => {
    const entries = fixture.componentInstance.newEntries;
    spyOn(entries, 'push');
    fixture.componentInstance.addEntry(exampleNewEntry);
    expect(entries.push).toHaveBeenCalled();
  });

  it('should play song after add Entry', () => {
    const sound = fixture.componentInstance.soundAfterNewMessage;
    spyOn(sound, 'play');
    fixture.componentInstance.addEntry(exampleNewEntry);
    expect(sound.play).toHaveBeenCalled();
  });

  it('should load song after add Entry', () => {
    const sound = fixture.componentInstance.soundAfterNewMessage;
    spyOn(sound, 'load');
    fixture.componentInstance.addEntry(exampleNewEntry);
    expect(sound.load).toHaveBeenCalled();
  });
});
