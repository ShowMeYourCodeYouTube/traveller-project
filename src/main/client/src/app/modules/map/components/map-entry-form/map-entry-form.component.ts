import {Component, ElementRef, NgZone, OnInit, ViewChild} from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {Localisation} from '../../../../shared/models/localisation';
import {MapsAPILoader} from '@agm/core';
import {MatDialogRef} from '@angular/material/dialog';

declare var google: any;

@Component({
  selector: 'app-map-entry-form',
  templateUrl: './map-entry-form.component.html',
  styleUrls: ['./map-entry-form.component.scss']
})
export class MapEntryFormComponent implements OnInit {

  @ViewChild('localisationSearch') localisationSearchElementRef: ElementRef;
  newEntryForm: FormGroup;

  constructor(
    public dialogRef: MatDialogRef<MapEntryFormComponent>,
    private mapsAPILoader: MapsAPILoader,
    private ngZone: NgZone) {
  }

  ngOnInit() {
    this.initializeForm();
    this.setAutocompleteLocalisation();
  }

  initializeForm() {
    this.newEntryForm = new FormGroup({
      name: new FormControl('', null, null),
      localisation: new FormGroup({
        place: new FormControl('', null, null),
        latitude: new FormControl('', null, null),
        longitude: new FormControl('', null, null),
      }),
      date: new FormControl('', null, null),
      description: new FormControl('', null, null)
    });
  }

  setAutocompleteLocalisation() {
    console.log('Loading...');
    this.mapsAPILoader.load().then(() => {
      const autocomplete = new google.maps.places.Autocomplete(this.localisationSearchElementRef.nativeElement);
      autocomplete.addListener('place_changed', () => {
        this.ngZone.run(() => {
          // @ts-ignore
          const place: google.maps.places.PlaceResult = autocomplete.getPlace();

          if (place.geometry === undefined || place.geometry === null) {
            return;
          }

          const addedLocalisation = new Localisation(place.formatted_address, place.geometry.location.lat(), place.geometry.location.lng());
          this.setLatitudeAndLongitude(addedLocalisation);
          // todo: improve
          // this.changePositionOnMap.emit(addedLocalisation);
        });
      });
    });
  }

  setLatitudeAndLongitude(loc: Localisation) {
    this.newEntryForm.controls['localisation'].get('latitude').setValue(loc.latitude);
    this.newEntryForm.controls['localisation'].get('longitude').setValue(loc.longitude);
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
}
