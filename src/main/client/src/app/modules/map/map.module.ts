import {NgModule} from '@angular/core';
import {MapRoutingModule} from './map.routing';
import {MapViewPage} from './pages/map-view/map-view.page';
import {ChatComponent} from './components/chat/chat.component';
import {MapComponent} from './components/map/map.component';
import {FormFilterComponent} from './components/form-filter/form-filter.component';
import {AgmCoreModule} from '@agm/core';
import {environment} from '../../../environments/environment';
import {ChatTimeDirective} from './directives/chat-time.directive';
import {SharedModule} from '../../shared/shared.module';
import {MapEntryFormComponent} from './components/map-entry-form/map-entry-form.component';
import {MapEntryFormWrapperComponent} from './components/map-entry-form-wrapper/map-entry-form-wrapper.component';

@NgModule({
  imports: [
    MapRoutingModule,
    SharedModule,
    AgmCoreModule.forRoot({
      apiKey: environment.apiKeyMaps,
      libraries: ['places']
    })
  ],
  declarations: [
    MapViewPage,
    ChatComponent,
    MapComponent,
    FormFilterComponent,
    ChatTimeDirective,
    MapEntryFormComponent,
    MapEntryFormWrapperComponent
  ],
  entryComponents: [
    MapEntryFormComponent
  ]
})
export class MapModule {
}
