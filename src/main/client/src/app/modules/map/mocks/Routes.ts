import {empty} from 'rxjs';

export class ActivatedRouteMock {
  params = empty();
}

export class RouterMock {
  navigate(): void {
  }
}
