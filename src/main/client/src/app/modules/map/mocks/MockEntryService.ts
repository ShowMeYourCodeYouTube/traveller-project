import {Injectable} from '@angular/core';
import {Entry} from '../../../shared/models/entry';
import {of} from 'rxjs';

@Injectable()
export class MockEntryService {

  readonly USER_ENTRIES: Array<Entry> = [
    {
      userId: 'exampleId',
      description: 'example description1',
      date: '2018-02-06',
      localisation: {
        place: 'Wroclaw',
        latitude: 51.1078852,
        longitude: 17.0385376
      },
      name: 'name1',
      isHidden: false
    },
    {
      userId: 'exampleId',
      description: 'example description2',
      date: '2018-02-16',
      localisation: {
        place: 'Szczecin',
        latitude: 53.428544,
        longitude: 14.552812
      },
      name: 'name2',
      isHidden: false
    }
  ];

  getUserEntries(userId: string) {
    return of(this.USER_ENTRIES);
  }

  addNewEntry(entry: Entry, userId: String) {
    return of(entry);
  }
}
