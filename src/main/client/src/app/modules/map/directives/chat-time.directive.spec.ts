import {ChatTimeDirective} from './chat-time.directive';

describe('ChatTimeDirective', () => {

  const mockElementRef = {
    nativeElement: {
      innerText: ''
    }
  };

  it('should create an instance', () => {
    const directive = new ChatTimeDirective(mockElementRef);
    expect(directive).toBeTruthy();
  });

  it('should add current time to element', () => {
    const directive = new ChatTimeDirective(mockElementRef);
    expect(mockElementRef.nativeElement.innerText.match('[0-9]{1,2}:[0-9]{2}')).toBeTruthy();
  });
});
