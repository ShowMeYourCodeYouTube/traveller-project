import {Directive, ElementRef} from '@angular/core';

@Directive({
  selector: '[appChatTime]'
})
export class ChatTimeDirective {

  private time: Date= new Date();

  constructor(el: ElementRef) {
    el.nativeElement.innerText = this.time.toLocaleTimeString([], {hour: '2-digit', minute: '2-digit'});
  }
}
