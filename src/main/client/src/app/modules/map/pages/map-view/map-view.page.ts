import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {WebsocketService} from 'app/core/services/websocket.service';
import {ChatComponent} from '../../components/chat/chat.component';
import {MapComponent} from '../../components/map/map.component';
import {Localisation} from '../../../../shared/models/localisation';
import {Subscription} from 'rxjs';
import {environment} from '../../../../../environments/environment';

@Component({
  selector: 'app-map-view',
  templateUrl: './map-view.page.html',
  styleUrls: ['./map-view.page.scss']
})
export class MapViewPage implements OnInit, OnDestroy {

  @ViewChild(MapComponent) mapView: MapComponent;
  @ViewChild(ChatComponent) chatView: ChatComponent;
  private subscription: Subscription = new Subscription();

  constructor(private websocketService: WebsocketService, private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.subscription.add(
      this.route.params.subscribe(params => this.initConnection(params['userId']))
    );
  }

  private initConnection(userId: string): Subscription {
    return this.websocketService.initializeWebSocketConnection()
    .subscribe(result => {
      console.log('WS connection initialized!');
      if (result) {
        this.websocketService.createSubscription(
          `${environment.urls.websocketChatTopic}/${userId}`,
          (message) => {
            this.chatView.addEntry(JSON.parse(message.body));
            this.mapView.addEntry(JSON.parse(message.body));
          });
      }
    }, error => {
      console.log('Cannot connect to WS! ', error)
    });
  }

  ngOnDestroy(): void {
    this.websocketService.disconnect();
    this.subscription.unsubscribe();
  }

  changeMapPositionHandler(localisation: Localisation) {
    this.mapView.setDefaultMapSettings(localisation.latitude, localisation.longitude);
  }

  showAllEntries() {
    this.mapView.showAllEntries();
  }
}
