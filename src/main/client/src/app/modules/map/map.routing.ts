import {RouterModule, Routes} from '@angular/router';
import {MapViewPage} from './pages/map-view/map-view.page';
import {NgModule} from '@angular/core';

const routes: Routes = [
  {path: ':userId', component: MapViewPage}
];

@NgModule({
  imports: [RouterModule.forChild(routes)]
})

export class MapRoutingModule {
}
