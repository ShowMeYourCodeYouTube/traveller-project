package com.traveller.listener;

import com.traveller.configuration.WebSocketConfiguration;
import lombok.RequiredArgsConstructor;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Component;

@RequiredArgsConstructor
@Component
public class WebSocketMessageService {

  private final SimpMessagingTemplate template;

  public void sendMessage(String id, String message) {
    var particularUserConnDestination = WebSocketConfiguration.CHAT_BROKER + "/" + id;
    template.convertAndSend(particularUserConnDestination, message);
  }
}
