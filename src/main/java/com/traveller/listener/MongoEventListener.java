package com.traveller.listener;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.traveller.entity.EntryEntity;
import java.util.Objects;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.bson.Document;
import org.springframework.data.mongodb.core.mapping.event.AbstractMongoEventListener;
import org.springframework.data.mongodb.core.mapping.event.AfterSaveEvent;
import org.springframework.stereotype.Component;

@Slf4j
@RequiredArgsConstructor
@Component
public class MongoEventListener extends AbstractMongoEventListener<EntryEntity> {

  private final WebSocketMessageService webSocketMessageService;

  @Override
  public void onAfterSave(AfterSaveEvent<EntryEntity> event) {
    Document document = event.getDocument();
    try {
      if (Objects.nonNull(document)) {
        webSocketMessageService.sendMessage(
            document.getString(EntryEntity.USER_ID),
            DocumentMapper.documentToEntryJson(document)
        );
      } else {
        log.warn("Document is null!");
      }
    } catch (JsonProcessingException e) {
      log.error("Cannot send a message! ", e);
    }
  }
}

