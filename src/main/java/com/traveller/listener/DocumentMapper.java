package com.traveller.listener;

import static com.traveller.entity.EntryEntity.DATE_FIELD;
import static com.traveller.entity.EntryEntity.DESCRIPTION_FIELD;
import static com.traveller.entity.EntryEntity.ENTRY_ID;
import static com.traveller.entity.EntryEntity.NAME_FIELD;
import static com.traveller.entity.EntryEntity.USER_ID;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.traveller.entity.EntryEntity;
import com.traveller.entity.LocalisationEntity;
import java.time.ZoneId;
import org.bson.Document;


public class DocumentMapper {

  private static final ObjectMapper mapper = new ObjectMapper();

  public static String documentToEntryJson(Document document) throws JsonProcessingException {
    EntryEntity e = new EntryEntity(
        document.getObjectId(ENTRY_ID),
        document.getString(USER_ID),
        document.getDate(DATE_FIELD).toInstant().atZone(ZoneId.systemDefault()).toLocalDate(),
        document.getString(NAME_FIELD),
        document.getString(DESCRIPTION_FIELD),
        documentToLocalisation(document.get("localisation", Document.class))
    );
    return mapper.writeValueAsString(e);
  }

  public static LocalisationEntity documentToLocalisation(Document document) {
    return new LocalisationEntity(
        document.getString("place"),
        document.getDouble("latitude"),
        document.getDouble("longitude")
    );
  }
}
