package com.traveller;

import com.traveller.entity.EntryEntity;
import com.traveller.entity.LocalisationEntity;
import com.traveller.entity.UserEntity;
import com.traveller.repository.EntryRepository;
import com.traveller.repository.UserRepository;
import java.time.LocalDate;
import java.util.Arrays;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.bson.types.ObjectId;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
@Slf4j
@AllArgsConstructor
public class ApplicationStartupRunner implements CommandLineRunner {

  private final UserRepository userRepository;
  private final EntryRepository entryRepository;

  @Override
  public void run(String... args) {
    log.info("Preparing the database with example data...");
    var allEntries = userRepository.findAll();
    if (allEntries.isEmpty()) {
      seedDatabase();
    }
  }

  private void seedDatabase() {
    ObjectId userId1 = new ObjectId("5ae47b387f71d71e00e8db5a");
    ObjectId userId2 = new ObjectId("5ae47b387f71d71e00e8db5b");
    userRepository.saveAll(Arrays.asList(
        new UserEntity(userId1, "Walter", "White"),
        new UserEntity(userId2, "Walter", "White"))
    );
    entryRepository.save(
        new EntryEntity(
            new ObjectId("62016f79a9adf1779d05e269"),
            userId1.toHexString(),
            LocalDate.parse("2018-05-16"),
            "Zakupy w niedziele",
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
            new LocalisationEntity("Wroclaw", 51.1119, 17.0678))
    );
    entryRepository.save(
        new EntryEntity(
            new ObjectId("62016f79a9adf1779d05e26a"),
            userId1.toHexString(),
            LocalDate.parse("2018-08-17"),
            "Odra",
            "Example description.",
            new LocalisationEntity("Wroclaw", 50.1119, 16.0678))
    );
    entryRepository.save(
        new EntryEntity(
            new ObjectId("62016f79a9adf1779d05e26b"),
            userId1.toHexString(),
            LocalDate.parse("2018-09-16"),
            "Biskupin",
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
            new LocalisationEntity("Wroclaw", 52.1119, 18.0678))
    );

    log.info("Test data successfully saved!");
  }

}
