package com.traveller.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
import org.springframework.web.socket.config.annotation.WebSocketMessageBrokerConfigurer;

@Configuration
@EnableWebSocketMessageBroker
public class WebSocketConfiguration implements WebSocketMessageBrokerConfigurer {

  public static final String CHAT_BROKER = "/chat";
  public static final String STOMP = "/app";
  public static final String SOCKJS_ENDPOINT = "/chat-messaging";

  @Override
  public void configureMessageBroker(MessageBrokerRegistry messageBrokerRegistry) {
    messageBrokerRegistry.setApplicationDestinationPrefixes(WebSocketConfiguration.STOMP);
    messageBrokerRegistry.enableSimpleBroker(WebSocketConfiguration.CHAT_BROKER);
  }

  @Override
  public void registerStompEndpoints(StompEndpointRegistry registry) {
    registry.addEndpoint(WebSocketConfiguration.SOCKJS_ENDPOINT)
        .setAllowedOriginPatterns("*")
        .withSockJS();
  }
}
