package com.traveller.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Field;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class LocalisationEntity {

  public static final String PLACE_FIELD = "place";
  public static final String LATITUDE_FIELD = "latitude";
  public static final String LONGITUDE_FIELD = "longitude";

  @Field(PLACE_FIELD)
  private String place;
  @Field(LATITUDE_FIELD)
  private double latitude;
  @Field(LONGITUDE_FIELD)
  private double longitude;
}


