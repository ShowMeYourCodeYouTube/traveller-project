package com.traveller.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;
import java.time.LocalDate;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "entries")
public class EntryEntity {

  public static final String ENTRY_ID = "_id";
  public static final String USER_ID = "userId";
  public static final String DATE_FIELD = "date";
  public static final String NAME_FIELD = "name";
  public static final String DESCRIPTION_FIELD = "description";
  public static final String LOCALISATION_FIELD = "localisation";

  @Id
  @Field(ENTRY_ID)
  @JsonSerialize(using = ToStringSerializer.class)
  private ObjectId entryId;
  @Field(USER_ID)
  private String userId;
  @Field(DATE_FIELD)
  @JsonDeserialize(using = LocalDateDeserializer.class)
  @JsonSerialize(using = LocalDateSerializer.class)
  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
  private LocalDate date;
  @Field(NAME_FIELD)
  private String name;
  @Field(DESCRIPTION_FIELD)
  private String description;
  @Field(LOCALISATION_FIELD)
  private LocalisationEntity localisation;
}
