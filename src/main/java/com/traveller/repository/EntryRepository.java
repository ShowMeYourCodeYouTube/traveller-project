package com.traveller.repository;

import com.traveller.entity.EntryEntity;
import java.util.List;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "entries", path = "entries")
public interface EntryRepository extends MongoRepository<EntryEntity, ObjectId> {

  List<EntryEntity> findByUserId(@Param("id") String id);
}
