package com.traveller.controller;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import com.traveller.IntegrationTestBase;
import com.traveller.entity.EntryEntity;
import com.traveller.entity.LocalisationEntity;
import com.traveller.repository.EntryRepository;
import com.traveller.test_util.TestResourcesLoader;
import java.time.LocalDate;
import org.bson.types.ObjectId;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public class RestDataControllerTest extends IntegrationTestBase {

  public static final LocalDate DATE = LocalDate.parse("2015-08-16");
  public static final String NAME = "Shopping TEST";
  public static final String DESCRIPTION = "Lorem ipsum dolor sit amet.";

  @Autowired
  private TestRestTemplate restTemplate;
  @Autowired
  private TestResourcesLoader testResourcesLoader;
  @Autowired
  private EntryRepository entryRepository;

  @Test
  public void shouldPostEntryUsingDataRest() {
    EntryEntity entity =
        new EntryEntity(null,
            new ObjectId(USER_ID).toHexString(),
            DATE,
            NAME,
            DESCRIPTION,
            new LocalisationEntity("Wroclaw", 51.1119, 17.0678)
        );

    HttpHeaders headers = new HttpHeaders();
    HttpEntity<EntryEntity> requestEntity = new HttpEntity<>(entity, headers);

    ResponseEntity<EntryEntity> responseEntity = restTemplate.exchange(
        "/entries",
        HttpMethod.POST,
        requestEntity,
        EntryEntity.class
    );

    assertThat(responseEntity.getStatusCode(), equalTo(HttpStatus.CREATED));
    assertNotNull(responseEntity.getBody());

    // cleanup
    entryRepository.delete(responseEntity.getBody());
  }

  @Test
  public void shouldGetEntriesUsingDataRest() {
    String expectedResource = testResourcesLoader.loadByFileName(TestResourcesLoader.ENTRIES_HAL);
    HttpHeaders headers = new HttpHeaders();
    headers.set(HttpHeaders.ACCEPT, "application/hal+json");
    HttpEntity<String> requestEntity = new HttpEntity<>(headers);

    ResponseEntity<String> response = restTemplate.exchange(
        "/entries",
        HttpMethod.GET,
        requestEntity,
        String.class
    );

    assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
    assertNotNull(response.getBody());
    assertEquals(removeWhitespaces(expectedResource), removeWhitespaces(response.getBody()));
  }

  @Test
  public void shouldGetEntriesForGivenUsingDataRest() {
    String expectedResource = testResourcesLoader.loadByFileName(
        TestResourcesLoader.ENTRIES_FOR_USER_HAL);
    HttpHeaders headers = new HttpHeaders();
    headers.set(HttpHeaders.ACCEPT, "application/hal+json");
    HttpEntity<String> requestEntity = new HttpEntity<>(headers);

    ResponseEntity<String> response = restTemplate.exchange(
        "/entries/search/findByUserId?id=5ae47b387f71d71e00e8db5a",
        HttpMethod.GET,
        requestEntity,
        String.class
    );

    assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
    assertNotNull(response.getBody());
    assertEquals(removeWhitespaces(expectedResource), removeWhitespaces(response.getBody()));
  }

  @Test
  public void shouldGetUsersUsingDataRest() {
    String expectedResource = testResourcesLoader.loadByFileName(TestResourcesLoader.USERS_HAL);
    HttpHeaders headers = new HttpHeaders();
    headers.set(HttpHeaders.ACCEPT, "application/hal+json");
    HttpEntity<String> requestEntity = new HttpEntity<>(headers);

    ResponseEntity<String> response = restTemplate.exchange(
        "/users",
        HttpMethod.GET,
        requestEntity,
        String.class
    );

    assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
    assertNotNull(response.getBody());
    assertEquals(removeWhitespaces(expectedResource), removeWhitespaces(response.getBody()));
  }

  private String removeWhitespaces(String item) {
    return item.replaceAll("\\s+", "");
  }
}
