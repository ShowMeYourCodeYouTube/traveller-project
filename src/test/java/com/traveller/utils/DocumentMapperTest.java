package com.traveller.utils;

import static com.traveller.entity.LocalisationEntity.LATITUDE_FIELD;
import static com.traveller.entity.LocalisationEntity.LONGITUDE_FIELD;
import static com.traveller.entity.LocalisationEntity.PLACE_FIELD;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;

import com.traveller.entity.LocalisationEntity;
import com.traveller.listener.DocumentMapper;
import org.bson.Document;
import org.hamcrest.number.IsCloseTo;
import org.junit.jupiter.api.Test;

public class DocumentMapperTest {

  @Test
  public void shouldMapDocumentToLocalisation() {

    Document localisationDocument = new Document();
    localisationDocument.put(PLACE_FIELD, "Moscow");
    localisationDocument.put(LATITUDE_FIELD, 12.243432);
    localisationDocument.put(LONGITUDE_FIELD, 10.123);

    LocalisationEntity result = DocumentMapper.documentToLocalisation(localisationDocument);

    assertEquals(localisationDocument.getString(PLACE_FIELD), result.getPlace());
    assertThat(localisationDocument.getDouble(LATITUDE_FIELD),
        new IsCloseTo(result.getLatitude(), 0.0));
    assertThat(localisationDocument.getDouble(LONGITUDE_FIELD),
        new IsCloseTo(result.getLongitude(), 0.0));
  }
}
