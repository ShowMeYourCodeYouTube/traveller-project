package com.traveller.test_util;

import static java.nio.charset.StandardCharsets.UTF_8;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UncheckedIOException;
import lombok.AllArgsConstructor;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Component;
import org.springframework.util.FileCopyUtils;

@AllArgsConstructor
@Component
public class TestResourcesLoader {

  public static String USERS_HAL = "users-hal.json";
  public static String ENTRIES_FOR_USER_HAL = "entries-for-5ae47b387f71d71e00e8db5a-hal.json";
  public static String ENTRIES_HAL = "entries-hal.json";
  private ResourceLoader resourceLoader;

  public String loadByFileName(String fileName) {
    Resource resource = resourceLoader.getResource("classpath:" + fileName);

    try (Reader reader = new InputStreamReader(resource.getInputStream(), UTF_8)) {
      return FileCopyUtils.copyToString(reader);
    } catch (IOException e) {
      throw new UncheckedIOException(e);
    }
  }
}
