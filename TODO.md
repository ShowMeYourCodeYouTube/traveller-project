# TODOs
- Upgrade libraries
- Add Docker setup
- review Angular Materials SDK flexbox etc.
- Fix running Jasmine tests
- Consider running Angular tests on CI pipeline
- class vs interface
- frontend move logic to services - do not use smart components
- formatting methods by visiblity
